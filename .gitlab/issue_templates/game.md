---
tags: [game, linux, windows, osx] # 👈 adjust platforms, add package tags
---

# Game Title <!-- 👈 set game title -->

> Game summary, preferably from its own page.
<!-- 👆 copy game summary here -->

- Download: <https://store.steampowered.com/app/0/Linky_Link/>
- Source: <https://forge.tld/project/page>
- Blog: <https://blog.example/url/Game/>
<!-- 👆 add relevant links -->

![current date](https://blog.example/images/screenshot.jpg)
<!-- 👆 mark screenshot with the current date>
