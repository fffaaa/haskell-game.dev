---
tags: [game, 2d, brick, osx, linux]
---

# Swarm

> Swarm is a 2D programming and resource gathering game.
> Program your robots to explore the world and collect resources, which in turn allows you to build upgraded robots that can run more interesting and complex programs.

- Blog: <https://byorgey.wordpress.com/2021/09/23/swarm-preview-and-call-for-collaboration/>
- Source: <https://github.com/byorgey/swarm>

![2021-12-20](https://github.com/byorgey/swarm/raw/main/images/tutorial/log.png)
