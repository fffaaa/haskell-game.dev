---
tags: [game, linux, windows, osx, sdl]
# XXX: platforms not tested, presumably should work on any SDL-supported env
---

# Treasure Dungeon

> Treasure dungeon is a dungeon crawling adventure game. You're an adventurer who seeks to explore an old and dangerous dungeon, with a goal of gathering as much loot as possible. The deeper you go, the greater the rewards (and dangers) are.

- Source: https://codeberg.org/tuturto/treasure-dungeon
- Blog: https://tech.lgbt/@tuturto/108069502094392640
- Gameplay video: https://tech.lgbt/@tuturto/108071777901350493

![2022-04-07](https://i.imgur.com/Y3BaqWK.jpg)
