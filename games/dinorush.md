---
tags: [game, 2d, sprites, sdl2, sdl2-mixer, osx, linux, windows]
---

# Dino Rush

> 🌋 Endless runner game

- Blog: <http://jxv.io/blog/2018-02-28-A-Game-in-Haskell.html>
- Source: <https://github.com/jxv/dino-rush>

![gameplay](https://raw.githubusercontent.com/jxv/dino-rush/master/propaganda/gameplay.gif)
