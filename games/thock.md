---
tags: [game, text, brick, linux, macos, multiplayer]
---

# THOCK

> A modern TUI typing test featuring online racing against friends.

- Source: https://github.com/rmehri01/thock

![2020-12-31](https://github.com/rmehri01/thock/raw/main/resources/demo.gif)
