---
tags: [game, 2d, sdl, fmod, linux, windows, osx]
---

# Defect Process

> Hack n' slash through increasingly dangerous arenas with a high action free-form combat system.

- Steam: <https://store.steampowered.com/app/1136730/Defect_Process/>
- Source: <https://github.com/incoherentsoftware/defect-process> (partial)
- Design: <https://incoherentsoftware.com/defect-process/docs/>

![2021-12-20](https://cdn.akamai.steamstatic.com/steam/apps/1136730/ss_42f8a6edea613feaa9de67f3916b7aeb89522345.1920x1080.jpg?t=1633462286)