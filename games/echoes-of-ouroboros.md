---
tags: [game, 2d, gloss, openal, linux, osx, windows, gamejam]
---

# Echoes of Ouroboros

- Source: <https://gitlab.com/macaroni.dev/gamedev-scratch/-/tree/LD47_1.0/ludum-dare/47-stuck-in-a-loop>
- Jam: <https://ldjam.com/events/ludum-dare/47/echoes-of-ouroboros>

![2020-11-29](https://img.itch.zone/aW1hZ2UvODAxMjkyLzQ0ODcxMTYucG5n/794x1000/XNbM4B.png)
