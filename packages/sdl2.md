---
tags: [package, 2d, sdl]
---

# SDL2

- Hackage: <https://hackage.haskell.org/package/sdl2>
- Source: <https://github.com/haskell-game/sdl2>

The package itself provides a nice introduction.

> SDL (Simple DirectMedia Layer) is a library for cross-platform development of interactive applications.
> SDL provides routines for managing windows, rendering graphics, processing sound, collecting input data, and much more.
> The Haskell `sdl2` library provides both a high- and low-level API to interface with SDL.

https://hackage.haskell.org/package/sdl2/docs/SDL.html

For an extended hands-on tutorial there is a Haskell port of LazyFoo's SDL lessons.

https://github.com/palf/haskell-sdl2-examples

## Minimal example

```haskell
module Main where

import SDL
import Linear (V4(..))
import Control.Monad (unless)

main :: IO ()
main = do
  initializeAll
  window <- createWindow "My SDL Application" defaultWindow
  renderer <- createRenderer window (-1) defaultRenderer
  appLoop renderer
  destroyWindow window

appLoop :: Renderer -> IO ()
appLoop renderer = do
  events <- pollEvents
  let eventIsQPress event =
        case eventPayload event of
          KeyboardEvent keyboardEvent ->
            keyboardEventKeyMotion keyboardEvent == Pressed &&
            keysymKeycode (keyboardEventKeysym keyboardEvent) == KeycodeQ
          _ -> False
      qPressed = any eventIsQPress events
  rendererDrawColor renderer $= V4 0 0 255 255
  clear renderer
  present renderer
  unless qPressed (appLoop renderer)
```

## Extras

- [sdl2-gfx](https://hackage.haskell.org/package/sdl2-gfx): Graphics primitives and surface functions.
- [sdl2-image](https://hackage.haskell.org/package/sdl2-image): Load images of various formats as SDL surfaces.
- [sdl2-mixer](https://hackage.haskell.org/package/sdl2-mixer): Multi-channel audio mixer.
- [sdl2-ttf](https://hackage.haskell.org/package/sdl2-ttf): FreeType wrapper to render text.

---

```query
tag: sdl
```
