---
page:
  title: Haskell GameDev
  siteTitle: ''
  description: "Yes, this is possible."
---

# Haskell GameDev

> Yes, this is possible.

!["8bit" by @astynax](HaskellGameDev.svg)

## Where to start

What kind of game do you want to make?

- 3D
  + [Vulkan](intro/vulkan.md) :book:
- 2D, sprites
  + [SDL](packages/sdl2.md)
- 2D, vectors
  + [Gloss](intro/gloss.md) :book:
- Text
  + [ansi-terminal-game](https://hackage.haskell.org/package/ansi-terminal-game)
  + [brick](https://hackage.haskell.org/package/brick)
  + [LamdaHack](https://github.com/LambdaHack/LambdaHack)
- Mobile
  + [Keera Hails](https://github.com/keera-studios/keera-hails)
- Mainstream game engine
  + [Godot](https://github.com/SimulaVR/godot-haskell)
- Experimental game engine
  + [Keid](https://keid.haskell-game.dev/) :book:

## Where to ask

- Boards
  + [Gamedev Reddit](https://www.reddit.com/r/haskellgamedev/)
- Chats
  + [Matrix](https://matrix.to/#/#haskell-game:matrix.org)
  * [IRC](ircs://irc.libera.chat:6697/#haskell-game): `#haskell-game` on libera
  + [Discord](https://discord.gg/vxpWtBA)
  + [Telegram](https://t.me/HaskellGameDev) (in russian)

## Historical links

Haskell has a deep gamedev history.
Despite most of the projects being "defunct or dormant" HaskellWiki provides a rich substrate for an aspiring code archeologist.

- https://wiki.haskell.org/Game_Development
